﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

public class Karakter : MonoBehaviourPunCallbacks
{
    public int p1score, p2score, ai = 200, girdim;
    public Text p1text, p2text;
    public bool bl, respawnbool, players2v2;
    public float can,canbitti, dumanint;
    public int skin, yedigim, attigim, vibri, fx, flagint, bayrakattim, respawnint, partnerinNosu, RakipCikmaInt,CikmaInt;
    public string plyr;
    public GameObject playernamego, playerpatlama, duman, myroket, respawntext, panelwon, panellost, camera, patlamam, partnerim, noktam;
    public Text durumtxt, bolgetxt,roomtxt;
    public Animator dumananim;
    public bool dumanbool, olumbool;
    public Sprite[] flags;
    public AudioSource youdied, wonses, failses, respawn;
    public Image lostflag, wonflag;


    void Start()
    {
        myroket.GetComponent<PhotonView>().RPC("ismim", RpcTarget.AllBuffered, photonView.Owner.ActorNumber.ToString());
        vibri = PlayerPrefs.GetInt("vibrate");
        if (!players2v2)
        {
            if (this.photonView.OwnerActorNr == 1)
            {
                this.transform.position = new Vector3(-19, 0, 0);
                this.transform.localEulerAngles = new Vector3(0, 0, -90);
            }
            else if (this.photonView.OwnerActorNr == 2)
            {
                this.transform.position = new Vector3(19, 0, 0);
                this.transform.localEulerAngles = new Vector3(0, 0, 90);
            }
        }
        else
        {
            noktam.gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
            if (this.photonView.OwnerActorNr == 1)
            {
                this.transform.position = new Vector3(-28.3f, -9.072731f, 0);
                this.transform.localEulerAngles = new Vector3(0, 0, -90);
            }    
            else if (this.photonView.OwnerActorNr == 3)
            {
                this.transform.position = new Vector3(-28f, 4.807269f, 0);
                this.transform.localEulerAngles = new Vector3(0, 0, -90);
            }
            else if (this.photonView.OwnerActorNr == 2)
            {
                this.transform.position = new Vector3(20, 4.847269f, 0);
                this.transform.localEulerAngles = new Vector3(0, 0, 90);
            }         
            else if (this.photonView.OwnerActorNr == 4)
            {
                this.transform.position = new Vector3(20, -9.052731f, 0);
                this.transform.localEulerAngles = new Vector3(0, 0, 90);
            }
        }
        canbitti = 150;
        if (photonView.IsMine) 
        {
            girdim = PlayerPrefs.GetInt("girdigim1");
            girdim += 1;
            PlayerPrefs.SetInt("girdigim1", girdim);
            this.gameObject.name = PhotonNetwork.LocalPlayer.NickName;
            fx = PlayerPrefs.GetInt("fx");
            if (fx == 1)
            {
                photonView.RPC("canayarla", RpcTarget.AllBuffered, 10);
            }
            if (fx == 7)
            {
                photonView.RPC("canayarla", RpcTarget.AllBuffered, 10);
            }
            if (fx == 8)
            {
                photonView.RPC("canayarla", RpcTarget.AllBuffered, 20);
            }
            if (fx == 9)
            {
                photonView.RPC("canayarla", RpcTarget.AllBuffered, 20);
            }
  
        }

    }
    [PunRPC]
    public void canayarla(int iqq)
    {
        this.GetComponent<Karakter>().can += iqq;
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (!players2v2)
        {
            durumtxt.text = "YOU          " + "WON";
            panelwon.gameObject.SetActive(true);
            this.GetComponent<KarakterKontrol>().enabled = false;
            this.GetComponent<SpriteRenderer>().enabled = false;
            this.GetComponent<BoxCollider2D>().enabled = false;
            myroket.gameObject.SetActive(false);
            playerpatlama.gameObject.SetActive(true);
            playerpatlama.GetComponent<Animator>().SetBool("expanimbool", true);
            panelwon.GetComponent<won_lost_classic>().paraal();
        }
        else
        {
            if(otherPlayer.ActorNumber == partnerinNosu)
            {
                this.GetComponent<KarakterKontrol>().hiz = this.GetComponent<KarakterKontrol>().hiz * 1.5f;
                myroket.GetComponent<roket>().hasar += 10;
                myroket.GetComponent<roket>().sarjor += 7;
                CikmaInt += 1;
            }
            else
            {
                RakipCikmaInt += 1;
                CikmaInt += 1;
                if(RakipCikmaInt == 2)
                {
                    durumtxt.text = "YOU          " + "WON";
                    panelwon.gameObject.SetActive(true);
                    this.GetComponent<KarakterKontrol>().enabled = false;
                    this.GetComponent<SpriteRenderer>().enabled = false;
                    this.GetComponent<BoxCollider2D>().enabled = false;
                    myroket.gameObject.SetActive(false);
                    playerpatlama.gameObject.SetActive(true);
                    playerpatlama.GetComponent<Animator>().SetBool("expanimbool", true);
                    panelwon.GetComponent<won_lost_classic>().paraal();
                }
            }
        }
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        PlayerPrefs.SetString("disconnectreason", cause.ToString());
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
        Application.LoadLevel("MainMenu");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!players2v2)
        {
            PhotonNetwork.KeepAliveInBackground = 120;
            if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
            {
                yedigim = this.GetComponent<scoreclassic>().p2score;
                attigim = this.GetComponent<scoreclassic>().p1score;
            }
            if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
            {
                yedigim = this.GetComponent<scoreclassic>().p1score;
                attigim = this.GetComponent<scoreclassic>().p2score;
            }
            roomtxt.text = PhotonNetwork.CurrentRoom.Name;

            if (can <= 0)
            {
                if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
                {
                    this.GetComponent<scoreclassic>().p2scorearttir(1);
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                {
                    this.GetComponent<scoreclassic>().p1scorearttir(1);
                }
                youdied.Play();
                durumtxt.text = "YOU  " + "DIED";
                can = 0;
                duman.GetComponent<duman>().enemyshooted.Stop();
                myroket.GetComponent<roket>().youhaveshoot.Stop();
                photonView.RPC("patlama", RpcTarget.AllBuffered, true, false);
                if (yedigim < 3)
                {
                    respawntext.SetActive(true);
                }
                canbitti--;
                if (canbitti == 0)
                {
                    if (yedigim < 3)
                    {
                        if (this.GetComponent<PhotonView>().Owner.ActorNumber == 1)
                        {

                            respawntext.SetActive(false);
                            this.GetComponent<PhotonView>().RPC("patlama", RpcTarget.AllBuffered, false, true);
                            this.transform.position = new Vector3(-19, 0, 0);
                            this.transform.localEulerAngles = new Vector3(0, 0, -90);
                            olumbool = true;
                            photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);

                        }
                        else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 2)
                        {

                            respawntext.SetActive(false);
                            this.GetComponent<PhotonView>().RPC("patlama", RpcTarget.AllBuffered, false, true);
                            this.transform.position = new Vector3(19, 0, 0);
                            this.transform.localEulerAngles = new Vector3(0, 0, 90);
                            olumbool = true;
                            photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                        }
                    }
                }
            }
            if (respawnbool)
            {
                respawnint--;
                if (respawnint == 110)
                {
                    respawn.Play();
                }
                if (respawnint == 0)
                {
                    if (this.GetComponent<PhotonView>().Owner.ActorNumber == 1)
                    {

                        respawntext.SetActive(false);
                        this.transform.position = new Vector3(-19, 0, 0);
                        this.transform.localEulerAngles = new Vector3(0, 0, -90);
                        photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);

                    }
                    else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 2)
                    {
                        respawntext.SetActive(false);
                        this.transform.position = new Vector3(19, 0, 0);
                        this.transform.localEulerAngles = new Vector3(0, 0, 90);
                        photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                    }
                    respawnbool = false;
                }
            }
            if (bayrakattim >= 3)
            {
                if (myroket.GetComponent<roket>().rakibim != null)
                {
                    myroket.GetComponent<roket>().rakibim.GetComponent<PhotonView>().RPC("CanYenile", RpcTarget.All, 0, 200);
                    if (this.GetComponent<PhotonView>().Owner.ActorNumber == 1)
                    {
                        this.GetComponent<scoreclassic>().p1score += 1;

                    }
                    else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 2)
                    {
                        this.GetComponent<scoreclassic>().p2score += 1;


                    }
                    Respawn();
                    bayrakattim = 0;
                }
            }
            if (yedigim >= 3)
            {
                can = 1;
                photonView.RPC("patlama", RpcTarget.AllBuffered, true, false);
                panellost.SetActive(true);

            }
            else if (attigim >= 3)
            {
                can = 1;
                photonView.RPC("patlama", RpcTarget.AllBuffered, true, false);
                panelwon.SetActive(true);
                panelwon.GetComponent<won_lost_classic>().paraal();
            }
            if (olumbool)
            {
                if (this.photonView.OwnerActorNr == 1 && photonView.IsMine)
                {
                    attigim = this.GetComponent<scoreclassic>().p1score;
                    yedigim = this.GetComponent<scoreclassic>().p2score;
                    olumbool = false;
                }
                if (this.photonView.OwnerActorNr == 2 && photonView.IsMine)
                {
                    attigim = this.GetComponent<scoreclassic>().p2score;
                    yedigim = this.GetComponent<scoreclassic>().p1score;
                    olumbool = false;
                }
            }
        }
        if (players2v2)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == 4)
            {
                if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
                {
                    if (GameObject.Find("3") != null)
                    {
                        partnerim = GameObject.Find("3").GetComponent<roket>().mykarakter;
                        partnerinNosu = 3;
                    }
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 3)
                {
                    if (GameObject.Find("1") != null)
                    {
                        partnerim = GameObject.Find("1").GetComponent<roket>().mykarakter;
                        partnerinNosu = 1;
                    }
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                {
                    if (GameObject.Find("4") != null)
                    {
                        partnerim = GameObject.Find("4").GetComponent<roket>().mykarakter;
                        partnerinNosu = 4;

                    }
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 4)
                {
                    if (GameObject.Find("2") != null)
                    {
                        partnerim = GameObject.Find("2").GetComponent<roket>().mykarakter;
                        partnerinNosu = 2;

                    }
                }
                partnerim.GetComponent<SpriteRenderer>().color = Color.green;
            }
            PhotonNetwork.KeepAliveInBackground = 120;
            if (PhotonNetwork.CurrentRoom.PlayerCount == 4 - CikmaInt)
            {
                if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
                {
                    yedigim = this.GetComponent<scoreclassic>().p2score + partnerim.GetComponent<scoreclassic>().p2score;
                    attigim = this.GetComponent<scoreclassic>().p1score + partnerim.GetComponent<scoreclassic>().p1score;
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 3)
                {
                    yedigim = this.GetComponent<scoreclassic>().p2score + partnerim.GetComponent<scoreclassic>().p2score;
                    attigim = this.GetComponent<scoreclassic>().p1score + partnerim.GetComponent<scoreclassic>().p1score;
                }

                if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                {
                    yedigim = this.GetComponent<scoreclassic>().p1score + partnerim.GetComponent<scoreclassic>().p1score;
                    attigim = this.GetComponent<scoreclassic>().p2score + partnerim.GetComponent<scoreclassic>().p2score;
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 4)
                {
                    yedigim = this.GetComponent<scoreclassic>().p1score + partnerim.GetComponent<scoreclassic>().p1score;
                    attigim = this.GetComponent<scoreclassic>().p2score + partnerim.GetComponent<scoreclassic>().p2score;
                }
            }
            else
            {
                if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
                {
                    yedigim = this.GetComponent<scoreclassic>().p2score + yedigim;
                    attigim = this.GetComponent<scoreclassic>().p1score + attigim;
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 3)
                {
                    yedigim = this.GetComponent<scoreclassic>().p2score + yedigim;
                    attigim = this.GetComponent<scoreclassic>().p1score + attigim;
                }

                if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                {
                    yedigim = this.GetComponent<scoreclassic>().p1score + yedigim;
                    attigim = this.GetComponent<scoreclassic>().p2score + attigim;
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 4)
                {
                    yedigim = this.GetComponent<scoreclassic>().p1score + yedigim;
                    attigim = this.GetComponent<scoreclassic>().p2score + attigim;
                }
            }
            roomtxt.text = PhotonNetwork.CurrentRoom.Name;

            if (can <= 0)
            {
                if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
                {
                    this.GetComponent<scoreclassic>().p2scorearttir(1);
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                {
                    this.GetComponent<scoreclassic>().p1scorearttir(1);
                }
                youdied.Play();
                durumtxt.text = "YOU  " + "DIED";
                can = 0;
                duman.GetComponent<duman>().enemyshooted.Stop();
                myroket.GetComponent<roket>().youhaveshoot.Stop();
                photonView.RPC("patlama", RpcTarget.AllBuffered, true, false);
                if (yedigim < 3)
                {
                    respawntext.SetActive(true);
                }
                canbitti--;
                if (canbitti == 0)
                {
                    if (yedigim < 10)
                    {
                        if (this.GetComponent<PhotonView>().Owner.ActorNumber == 1)
                        {
                            respawntext.SetActive(false);
                            this.GetComponent<PhotonView>().RPC("patlama", RpcTarget.AllBuffered, false, true);
                            this.transform.position = new Vector3(-28.3f, -9.072731f, 0);
                            this.transform.localEulerAngles = new Vector3(0, 0, -90);
                            olumbool = true;
                            photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                        }
                        else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 2)
                        {
                            respawntext.SetActive(false);
                            this.GetComponent<PhotonView>().RPC("patlama", RpcTarget.AllBuffered, false, true);
                            this.transform.position = new Vector3(20, 4.847269f, 0);
                            this.transform.localEulerAngles = new Vector3(0, 0, 90);
                            olumbool = true;
                            photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                        }
                        else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 3)
                        {
                            respawntext.SetActive(false);
                            this.GetComponent<PhotonView>().RPC("patlama", RpcTarget.AllBuffered, false, true);
                            this.transform.position = new Vector3(-28f, 4.807269f, 0);
                            this.transform.localEulerAngles = new Vector3(0, 0, 90);
                            olumbool = true;
                            photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                        }
                        else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 4)
                        {
                            respawntext.SetActive(false);
                            this.GetComponent<PhotonView>().RPC("patlama", RpcTarget.AllBuffered, false, true);
                            this.transform.position = new Vector3(20, -9.052731f, 0);
                            this.transform.localEulerAngles = new Vector3(0, 0, 90);
                            olumbool = true;
                            photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                        }
                    }
                }
               
            }
           
            if (respawnbool)
            {
                respawnint--;
                if (respawnint == 110)
                {
                    respawn.Play();
                }
                if (respawnint == 0)
                {
                    if (this.GetComponent<PhotonView>().Owner.ActorNumber == 1)
                    {

                        respawntext.SetActive(false);
                        this.transform.position = new Vector3(-28.3f, -9.072731f, 0);
                        this.transform.localEulerAngles = new Vector3(0, 0, -90);
                        photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);

                    }
                    else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 2)
                    {
                        respawntext.SetActive(false);
                        this.transform.position = new Vector3(20, 4.847269f, 0);
                        this.transform.localEulerAngles = new Vector3(0, 0, 90);
                        photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                    }
                    else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 3)
                    {
                        respawntext.SetActive(false);
                        this.transform.position = new Vector3(-28f, 4.807269f, 0);
                        this.transform.localEulerAngles = new Vector3(0, 0, 90);
                        photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                    }
                    else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 4)
                    {
                        respawntext.SetActive(false);
                        this.transform.position = new Vector3(20, -9.052731f, 0);
                        this.transform.localEulerAngles = new Vector3(0, 0, 90);
                        photonView.RPC("CanYenile", RpcTarget.AllBuffered, 100, 200);
                    }
                    respawnbool = false;
                }
            }
            if (yedigim >= 10)
            {
                can = 1;
                photonView.RPC("patlama", RpcTarget.AllBuffered, true, false);
                panellost.SetActive(true);

            }
            else if (attigim >= 10)
            {
                can = 1;
                photonView.RPC("patlama", RpcTarget.AllBuffered, true, false);
                panelwon.SetActive(true);
                panelwon.GetComponent<won_lost_classic>().paraal();
            }
            if (olumbool) 
            {
                if (this.photonView.OwnerActorNr == 1 && photonView.IsMine)
                {
                    attigim = this.GetComponent<scoreclassic>().p1score + partnerim.GetComponent<scoreclassic>().p1score;
                    yedigim = this.GetComponent<scoreclassic>().p2score + partnerim.GetComponent<scoreclassic>().p2score;
                    olumbool = false;
                } 
                if (this.photonView.OwnerActorNr == 3 && photonView.IsMine)
                {
                    attigim = this.GetComponent<scoreclassic>().p1score + partnerim.GetComponent<scoreclassic>().p1score;
                    yedigim = this.GetComponent<scoreclassic>().p2score + partnerim.GetComponent<scoreclassic>().p2score;
                    olumbool = false;
                }
                if (this.photonView.OwnerActorNr == 2 && photonView.IsMine)
                {
                    attigim = this.GetComponent<scoreclassic>().p2score + partnerim.GetComponent<scoreclassic>().p2score;
                    yedigim = this.GetComponent<scoreclassic>().p1score + partnerim.GetComponent<scoreclassic>().p1score;
                    olumbool = false;
                }
                if (this.photonView.OwnerActorNr == 4 && photonView.IsMine)
                {
                    attigim = this.GetComponent<scoreclassic>().p2score + partnerim.GetComponent<scoreclassic>().p2score;
                    yedigim = this.GetComponent<scoreclassic>().p1score + partnerim.GetComponent<scoreclassic>().p1score;
                    olumbool = false;
                }
            }
        }
            if (photonView.IsMine)
            {
                this.gameObject.name = PhotonNetwork.LocalPlayer.NickName;
                playernamego.GetComponent<TextMesh>().text = PhotonNetwork.LocalPlayer.NickName + " hp: " + can.ToString();
                wonflag.GetComponent<Image>().sprite = this.gameObject.GetComponent<skin>().flags[PlayerPrefs.GetInt("flag")];
                lostflag.GetComponent<Image>().sprite = this.gameObject.GetComponent<skin>().flags[PlayerPrefs.GetInt("flag")];
            }
            else
            {
                playernamego.GetComponent<TextMesh>().text = photonView.Owner.NickName;
            }
           
            plyr = PhotonNetwork.LocalPlayer.ActorNumber.ToString();

        
    }
    public void Respawn()
    {
        respawntext.SetActive(true);
        respawnint = 200;
        respawnbool = true;
    }

    [PunRPC]
    public void TakeDamage(float damage,bool aqq)
    {
        camera.GetComponent<Animator>().Play("camvibration");
        patlamam.gameObject.SetActive(aqq);
        this.GetComponent<AudioSource>().Play();
        can -= damage;
        if (vibri == 0)
        {
            Handheld.Vibrate();
        }


    }
    [PunRPC]
    public void CanYenile(int cani, int bitti)
    {
        can = cani;
        canbitti = bitti;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name != plyr & other.gameObject.tag == "Roket")
        {
            patlamam.GetComponent<ParticleSystem>().Play();
            Debug.Log(other.GetComponent<roket>().hasar);
        }
        if (this.name != plyr)
        {
            if (photonView.IsMine)
            {
                if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
                {
                    if (flagint == 0)
                    {
                        if (other.gameObject.name == "flag2")
                        {
                            other.GetComponent<PhotonView>().RPC("flagkapat", RpcTarget.All, false);
                            flagint = 1;
                        }
                    }
                    if (flagint == 1)
                    {
                        if (other.gameObject.name == "for1")
                        {
                            bayrakattim += 1;
                            other.gameObject.GetComponent<PhotonView>().RPC("flagayar", RpcTarget.All, true);
                            other.gameObject.GetComponent<PhotonView>().RPC("flagkapat2", RpcTarget.All, false);
                            flagint = 0;

                        }
                    }
                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                {
                    if (flagint == 0)
                    {
                        if (other.gameObject.name == "flag1")
                        {
                            other.GetComponent<PhotonView>().RPC("flagkapat", RpcTarget.All, false);
                            flagint = 1;
                        }
                    }
                    if (flagint == 1)
                    {
                        if (other.gameObject.name == "for2")
                        {
                            bayrakattim += 1;
                            other.gameObject.GetComponent<PhotonView>().RPC("flagayar", RpcTarget.All, true);
                            other.gameObject.GetComponent<PhotonView>().RPC("flagkapat2", RpcTarget.All, false);
                            flagint = 0;


                        }
                    }
                }
            }
        }
        if(other.gameObject.name == "middle")
        {
            bolgetxt.text = "MIDDLE";
        }
        if (other.gameObject.name == "tunnelright")
        {
            bolgetxt.text = "RIGHT TUNNEL";
        }
        if (other.gameObject.name == "tunnelleft")
        {
            bolgetxt.text = "LEFT TUNNEL";
        }
        if (other.gameObject.name == "warzoneright")
        {
            bolgetxt.text = "RIGHT WARZONE";
        }
        if (other.gameObject.name == "warzoneleft")
        {
            bolgetxt.text = "LEFT WARZONE";
        }
        if (other.gameObject.name == "uppermiddle")
        {
            bolgetxt.text = "UPPER MIDDLE";
        }
        if (other.gameObject.name == "lowermiddle")
        {
            bolgetxt.text = "LOWER MIDDLE";
        }
        if (other.gameObject.name == "leftlowerlong")
        {
            bolgetxt.text = "LEFT LOWER LONG";
        }
        if (other.gameObject.name == "leftupperlong")
        {
            bolgetxt.text = "LEFT UPPER LONG";
        }
        if (other.gameObject.name == "rightupperlong")
        {
            bolgetxt.text = "RIGHT UPPER LONG";
        }
        if (other.gameObject.name == "rightlowerlong")
        {
            bolgetxt.text = "RIGHT LOWER LONG";
        }
        if (other.gameObject.name == "rightshort")
        {
            bolgetxt.text = "RIGHT SHORT";
        }
        if (other.gameObject.name == "leftshort")
        {
            bolgetxt.text = "LEFT SHORT";
        }
    }
   
    [PunRPC]
    public void patlama(bool bl1, bool bl2)
    {
        playerpatlama.gameObject.SetActive(bl1);
        playerpatlama.GetComponent<Animator>().SetBool("expanimbool", bl1);
        this.GetComponent<KarakterKontrol>().enabled = bl2;
        this.GetComponent<SpriteRenderer>().enabled = bl2;
        this.GetComponent<BoxCollider2D>().enabled = bl2;
        myroket.GetComponent<roket>().enabled = bl2;
    }

   

}
