﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class GameManagerOfGame : MonoBehaviourPunCallbacks
{

    [SerializeField]
    GameObject playerPrefab;

    void Start()
    {
        if (PhotonNetwork.IsConnected)
        {
            if (playerPrefab != null)
            {
                int randompointx = Random.Range(-39, 0);
                int randompointy = Random.Range(-23, -26);
                PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(randompointx, randompointy, 0), Quaternion.identity);
            }

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.NickName + "  joined to  " + PhotonNetwork.CurrentRoom.Name);

    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.NickName + "  joined to  " + PhotonNetwork.CurrentRoom.Name + " " + PhotonNetwork.CurrentRoom.PlayerCount + " player(s) activated. ");

    }
   
}
