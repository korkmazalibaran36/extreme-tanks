﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class referans : MonoBehaviour
{
    public string kodum;
    public GameObject inputf, kodimg,unlockimg, butonum;
    public Text wrongtxt;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        kodum = inputf.GetComponent<Text>().text;
        int a = PlayerPrefs.GetInt("referans");
        if (a == 1)
        {
            butonum.gameObject.SetActive(false);
        }
    }
    public void kodalma()
    {
        if(kodum == "abk11")
        {
            PlayerPrefs.SetInt("referans", 1);
            int money = PlayerPrefs.GetInt("gem");
            money += 5000;
            PlayerPrefs.SetInt("gem", money);
            kodimg.SetActive(false);
        }
        else if(kodum == "tusastr")
        {
            PlayerPrefs.SetInt("referans", 1);
            int money = PlayerPrefs.GetInt("gem");
            money += 5000;
            PlayerPrefs.SetInt("gem", money);
            kodimg.SetActive(false);
        }
        else
        {
            wrongtxt.text = "CODE IS WRONG!";
        }
    }
    public void imgackapa(bool acikmi)
    {
        kodimg.gameObject.SetActive(acikmi);
    }
    public void unlockimgackapa(bool acikmi)
    {
        unlockimg.gameObject.SetActive(acikmi);
        PlayerPrefs.SetInt("choosed", 1);
    }
    public void paylas()
    {
        Application.OpenURL("https://lastbloodgaming.blogspot.com/2021/03/2020/10/tanksio-multiplayer.html");
    }
}
