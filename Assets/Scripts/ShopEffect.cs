﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopEffect : MonoBehaviour
{
    public GameObject acilacak, acilacakCanvas, kapanacak, kapanacakCanvas;
    public GameObject camAc, camKapa;
    void Start()
    {


    }

    void Update()
    {
        
    }
    public void ac_kapa()
    {
        acilacak.gameObject.SetActive(true);
        acilacakCanvas.gameObject.SetActive(true);
        kapanacak.gameObject.SetActive(false);
        kapanacakCanvas.gameObject.SetActive(false);
    }
    public void CameraControl()
    {
        camAc.SetActive(true);
        camKapa.SetActive(false);
    }
}
