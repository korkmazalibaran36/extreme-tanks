﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerNameInputManager : MonoBehaviour
{
    public Text textt;

    void Start()
    {
        this.GetComponent<InputField>().text = PlayerPrefs.GetString("nick");
    }
    public void SetPlayerName(string playername)
    {
        playername = textt.text;

        if (string.IsNullOrEmpty(playername))
        {
            Debug.Log("Player Name Is Empty");
            return;
        }
        PlayerPrefs.SetString("nick",playername);
        PhotonNetwork.NickName = playername+"";
    }
   
    void Update()
    {

    }

}

