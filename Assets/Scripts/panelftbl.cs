﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class panelftbl : MonoBehaviourPunCallbacks
{
    public GameObject mykarakter;
    public GameObject load1, load2, load3, load4, load5, panelload,modetxt;
    public Text loadingtext, loadingtext2, nicktext;
    public Sprite sprite1, sprite2, sprite3, sprite4, sprite5;
    public int haziroyuncu, time, a;
    public Text timetxt, roomtxt;

    void Start()
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
        {
            a = 150;
        }
        if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
        {
            a = 130;
        }
        nicktext.text = "@Player" + PhotonNetwork.LocalPlayer.ActorNumber;
        this.gameObject.SetActive(true);
       mykarakter.GetComponent<fut_Kontrol>().enabled = false;
       mykarakter.GetComponent<SpriteRenderer>().enabled = false;
       mykarakter.GetComponent<BoxCollider2D>().enabled = false;
        roomtxt.text = PhotonNetwork.CurrentRoom.Name;

    }

    void FixedUpdate()
    {
        if (haziroyuncu != 2)
        {
            timetxt.text = (time / 60).ToString();
        }
        time--;
        if (time <= 0)
        {
            if (haziroyuncu != 2)
            {
                time = 0;
                quit();
            }
        }
        haziroyuncu = PhotonNetwork.CurrentRoom.PlayerCount;
        if (haziroyuncu == 2)
        {
            a--;
            if (a == 119)
            {
                load1.GetComponent<Image>().sprite = sprite1;
                load2.GetComponent<Image>().sprite = sprite2;
                load3.GetComponent<Image>().sprite = sprite3;
                load4.GetComponent<Image>().sprite = sprite4;
                load5.GetComponent<Image>().sprite = sprite5;
                loadingtext.text = "ALL PLAYERS CONNECTED!";
                loadingtext2.text = "STARTING...";
            }
            if (a == 50)
            {
                this.GetComponent<AudioSource>().Play();
                load3.GetComponent<Animator>().enabled = true;
                load4.GetComponent<Animator>().enabled = true;
                loadingtext.gameObject.SetActive(false);
                loadingtext2.gameObject.SetActive(false);
                modetxt.gameObject.SetActive(false);
                nicktext.gameObject.SetActive(false);
            }
            if (a == 20)
            {
                load1.GetComponent<Animator>().enabled = true;
                load2.GetComponent<Animator>().enabled = true;
                load5.GetComponent<Animator>().enabled = true;
                mykarakter.GetComponent<fut_Kontrol>().enabled = true;
                mykarakter.GetComponent<SpriteRenderer>().enabled = true;
                mykarakter.GetComponent<BoxCollider2D>().enabled = true;

            }
            if (a == -30)
            {
                panelload.gameObject.SetActive(false);
            }

        }

    }
    
    public void quit()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
        Application.LoadLevel("MainMenu");
    }
    
}
