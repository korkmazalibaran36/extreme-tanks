﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fut_roket : MonoBehaviourPunCallbacks
{
    public bool vur;
    public int a = 30, sarjor,vibri;
    public Animator rokanim;
    public float hasar, para, para_a;
    public GameObject mykarakter, butonvurus,top, camera;
    public Text sarjortxt, paratxt, durumtxt;
    public AudioSource ahh, goall;

    void Start()
    {
        vibri = PlayerPrefs.GetInt("vibrate");
        sarjor = 10;
        top = GameObject.Find("topoyun");

    }
    // [PunRPC]
    public void FixedUpdate()
    {
        sarjor = mykarakter.GetComponent<fut_Karakter>().mermim;
        para_a = hasar * 1.5f;
        para = PlayerPrefs.GetFloat("money");
        paratxt.text = para.ToString();

        sarjortxt.text = sarjor.ToString();
        if (vur)
        {
            butonvurus.GetComponent<Button>().interactable = false;
            a--;
            if (a == 0)
            {
                rokanim.SetBool("animbool", false);

            }
            if (a < 0)
            {
                butonvurus.GetComponent<Button>().interactable = true;
                vur = false;

            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            vurhedef();
        }

    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "kale")
        {
            if (other.gameObject.name != "direk")
            {
                camera.GetComponent<Animator>().Play("camvibration");
                if (vibri == 0)
                {
                    Handheld.Vibrate();
                }
                if (this.gameObject.name == "1")
                {
                    goall.Play();
                    Debug.Log("değdi");
                    rokanim.SetBool("animbool", false);
                    rokanim.Play("New State");
                    if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
                    {
                        other.gameObject.GetComponent<PhotonView>().RPC("p1golatti", RpcTarget.All);
                        Debug.Log("aa");
                    }

                }
                if (this.gameObject.name == "2")
                {
                    goall.Play();
                    Debug.Log("değdi");
                    rokanim.SetBool("animbool", false);
                    rokanim.Play("New State");

                    if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                    {
                        other.gameObject.GetComponent<PhotonView>().RPC("p2golatti", RpcTarget.All);
                        Debug.Log("bb");
                    }
                }
            }

        }
         if (other.gameObject.name == "direk")
        {
            ahh.Play();
            rokanim.SetBool("animbool", false);
            rokanim.Play("New State");

        }
        if (other.gameObject.name == "kaleci")
        {
            ahh.Play();
            rokanim.SetBool("animbool", false);
            rokanim.Play("New State");

        }
        if (other.gameObject.tag == "Player")
        {
            if(other.gameObject.name == "Player")
            ahh.Play();
            rokanim.SetBool("animbool", false);
            rokanim.Play("New State");

        }

    }
    //[PunRPC]
    public void vurhedef()
    {
        if (sarjor > 0)
        {
            top.GetComponent<PhotonView>().RPC("top_aktivate", RpcTarget.All, true);
            mykarakter.GetComponent<fut_Karakter>().mermim -= 1;
            Debug.Log(this.name);
            rokanim.SetBool("animbool", true);
            a = 30;
            vur = true;
        }
    }

}
