﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;


public class skin : MonoBehaviourPunCallbacks
{
    public Sprite[] flags;
    public int myskin,country;
    public Sprite[] mysprite;
    bool aqq;



    void Start()
    {
      
       
    }

    void FixedUpdate()
    {
        if (aqq == false)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
            {
                if (photonView.IsMine)
                {
                    country = PlayerPrefs.GetInt("flag");
                    myskin = PlayerPrefs.GetInt("skin");
                    photonView.RPC("skina", RpcTarget.AllBuffered, true, myskin);
                    if (this.tag == "Player")
                    {
                        photonView.RPC("flaga", RpcTarget.AllBuffered, country);
                    }
                    aqq = true;
                }
            }
        }

    }
   [PunRPC]
    public void flaga(int countryi)
    {
        if (this.GetComponent<PhotonView>().Owner.ActorNumber == 1)
        {
            GameObject.Find("p1flag").GetComponent<SpriteRenderer>().sprite = flags[countryi];
            GameObject.Find("p1scoreflag").GetComponent<Image>().sprite = flags[countryi];
            GameObject.Find("p1flag").GetComponent<SpriteRenderer>().size = new Vector2(4.3f, 2.5f);
        }
        else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 2)
        {
            GameObject.Find("p2flag").GetComponent<SpriteRenderer>().sprite = flags[countryi];
            GameObject.Find("p2scoreflag").GetComponent<Image>().sprite = flags[countryi];
            GameObject.Find("p2flag").GetComponent<SpriteRenderer>().size = new Vector2(4.3f, 2.5f);
        }
        else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 3)
        {
            GameObject.Find("p1flag (1)").GetComponent<SpriteRenderer>().sprite = flags[countryi];
            GameObject.Find("p1scoreflag (1)").GetComponent<Image>().sprite = flags[countryi];
            GameObject.Find("p1flag (1)").GetComponent<SpriteRenderer>().size = new Vector2(4.3f, 2.5f);
        }
        else if (this.GetComponent<PhotonView>().Owner.ActorNumber == 4)
        {
            GameObject.Find("p2flag (1)").GetComponent<SpriteRenderer>().sprite = flags[countryi];
            GameObject.Find("p2scoreflag (1)").GetComponent<Image>().sprite = flags[countryi];
            GameObject.Find("p2flag (1)").GetComponent<SpriteRenderer>().size = new Vector2(4.3f, 2.5f);
        }
    }
   
    [PunRPC]
    public void skina(bool annen, int skincek)
    {
        this.GetComponent<SpriteRenderer>().sprite = mysprite[skincek];

    }

}
