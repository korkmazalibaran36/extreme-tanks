﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice.Unity;
using Photon.Voice.PUN;


public class Voice : MonoBehaviour
{
    public Recorder VoiceRecorder;
    private PhotonView pv;
    public bool konusuyorum, test;
    public GameObject button;
    public Sprite aciks, kapalis, bastaki;

    void Start()
    {
        konusuyorum = true;
        pv = this.GetComponent<PhotonView>();
        VoiceRecorder.TransmitEnabled = false;
    }

    void Update()
    {
        if (!test)
        {
            button.GetComponent<Image>().sprite = bastaki;
        }
        if (konusuyorum && test)
        {
            button.GetComponent<Image>().sprite = aciks;
        }
        else if(!konusuyorum && test)
        {
            button.GetComponent<Image>().sprite = kapalis;
        }
    }
    public void Basildi()
    {
        VoiceRecorder.TransmitEnabled = konusuyorum;
        konusuyorum = !konusuyorum;
        test = true;

    }
}

