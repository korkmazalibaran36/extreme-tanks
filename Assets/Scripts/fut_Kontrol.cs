﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;

public class fut_Kontrol : MonoBehaviour
{
    public Camera maincam;
    public float hiz, hizkontrol;
    public bool ileribool, sagbool, solbool, ilerigeri, oyuncutamamlandi,speedbl, kalecibool;
    public GameObject ilerivites, gerivites,waitpanel, powerspeedgo, butonpower1,nitro, kaleci;
    public int skin, speed5, kaleciint, fx;

    void Start()
    {
        fx = PlayerPrefs.GetInt("fx");
        skin = PlayerPrefs.GetInt("skin");
        ilerigeri = true;
        if (skin == 0)
        {
            hiz = 0.2f;
        }
        if (skin == 1)
        {
            hiz = 0.2f;
        }
        if (skin == 2)
        {
            hiz = 0.22f;
        }
        if (skin == 3)
        {
            hiz = 0.25f;
        }
        if (skin == 4)
        {
            hiz = 0.25f;
        }
        if (skin == 5)
        {
            hiz = 0.27f;
        }
        if (skin == 6)
        {
            hiz = 0.28f;
        }
        if (skin == 7)
        {
            hiz = 0.28f;
        }
        if (skin == 8)
        {
            hiz = 0.3f;
        }
        if (skin == 9)
        {
            hiz = 0.3f;
        }
        if (fx == 3)
        {
            hiz += 0.06f;
        }
        if (fx == 8)
        {
            hiz += 0.04f;
        }
        hizkontrol = hiz;
    }

    void FixedUpdate()
    {
        kaleci = GameObject.Find("kaleci");
        if (kalecibool)
        {
            kaleciint--;
            if(kaleciint <= 0)
            {
                kaleci.GetComponent<PhotonView>().RPC("superguc", RpcTarget.All, false);
                kalecibool = false;

            }
        }
        if (speedbl)
        {
            butonpower1.SetActive(false);
            powerspeedgo.SetActive(true);
            nitro.SetActive(true);
            speed5--;
            if(speed5 <= 0)
            {
                hizkontrol = hiz;
                powerspeedgo.SetActive(false);
                nitro.SetActive(false);
                speedbl = false;
                
            }
        }
        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2(0, moveVertical);
        this.transform.Translate(movement * (hiz));

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, 0, 1.5f);

        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 0, -1.5f);

        }
    
        if (ileribool)
        {
            if (ilerigeri == true)
            {
                transform.Translate(0, hizkontrol, 0);

            }
            else
            {
                transform.Translate(0, -hizkontrol / 1.5f, 0);

            }
        }
        if (sagbool)
        {
            transform.Rotate(0, 0, -1.5f);
        }
        if (solbool)
        {
            transform.Rotate(0, 0, 1.5f);
        }


    }
    public void powerspeed()
    {
        float money = PlayerPrefs.GetFloat("money");
        if (money >= 100)
        {
            money -= 100;
            PlayerPrefs.SetFloat("money", money);
            hizkontrol = hiz * 2f;
            speedbl = true;
            speed5 = 300;
        }
    }
    public void kaleciaktif()
    {
        float money = PlayerPrefs.GetFloat("money");
        if (money >= 100)
        {
            money -= 100;
            PlayerPrefs.SetFloat("money", money);
            kaleci.GetComponent<PhotonView>().RPC("superguc", RpcTarget.All, true);
            kalecibool = true;
            kaleciint = 300;
        }
    }
    public void ileri()
    {
        ileribool = true;
    }
    public void ileribirak()
    {
        ileribool = false;
    }
    public void solbirak()
    {
        solbool = false;
    }
    public void sagbirak()
    {
        sagbool = false;
    }

    public void donmesag()
    {
        sagbool = true;
    }
    public void donmesol()
    {
        solbool = true;
    }
    public void vitesayarla()
    {
        if (ilerigeri == true)
        {
            gerivites.gameObject.SetActive(true);
            ilerivites.gameObject.SetActive(false);
            ilerigeri = false;
        }
        else if (ilerigeri == false)
        {
            gerivites.gameObject.SetActive(false);
            ilerivites.gameObject.SetActive(true);
            ilerigeri = true;
        }
    }
}
