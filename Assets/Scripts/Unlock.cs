﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Unlock : MonoBehaviour
{
    public int modei;
    public GameObject btn1, btn2, panelunlock, buttonunlock;
    public Text reklam_modetxt, unlocktext;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        modei = PlayerPrefs.GetInt("UnlockModes");
        reklam_modetxt.text = modei.ToString()  +  " / 3";
        
        if(modei == 3)
        {
            btn1.GetComponent<Button>().interactable = true;
            btn2.GetComponent<Button>().interactable = true;
            panelunlock.SetActive(false);
            buttonunlock.SetActive(false);
            unlocktext.text = "HEY DUDE, THIS GAME IS NEW. IF YOU COULD NOT FIND ENEMY, SHARE THE GAME WITH YOUR FRIEND AND PLAY WITH THEM!!"; 

        }
    }
}
