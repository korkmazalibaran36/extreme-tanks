﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setting_main : MonoBehaviour
{
    public Slider mainsfx, ingamemusic, shootsfx, radiosfx, footballgoal;
    public Text statu_vibrate;
    public int vibr_i, ilkmi;

    void Start()
    {
        ilkmi = PlayerPrefs.GetInt("ilkmi");
        if(ilkmi == 0)
        {
            PlayerPrefs.SetFloat("money", 1000);
            PlayerPrefs.SetInt("vibrate",1);
            PlayerPrefs.SetInt("ilkmi", 1);
        }
        if (PlayerPrefs.GetInt("acildionce") == 1)
        {
            mainsfx.value = PlayerPrefs.GetFloat("mainsfx");
            ingamemusic.value = PlayerPrefs.GetFloat("ingamemusic");
            shootsfx.value = PlayerPrefs.GetFloat("shootsfx");
            radiosfx.value = PlayerPrefs.GetFloat("radiosfx");
            footballgoal.value = PlayerPrefs.GetFloat("footballgoal");
        }
        PlayerPrefs.SetInt("acildionce", 1); 
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        vibr_i = PlayerPrefs.GetInt("vibrate");
        if(vibr_i == 0)
        {
            statu_vibrate.text = "ON";
        }
        else if (vibr_i == 1)
        {
            statu_vibrate.text = "OFF";
        }
        PlayerPrefs.SetFloat("mainsfx", mainsfx.value);
        PlayerPrefs.SetFloat("ingamemusic", ingamemusic.value);
        PlayerPrefs.SetFloat("shootsfx", shootsfx.value);
        PlayerPrefs.SetFloat("radiosfx", radiosfx.value);
        PlayerPrefs.SetFloat("footballgoal", footballgoal.value);
    }
    public void vibrate_ac()
    {
        PlayerPrefs.SetInt("vibrate", 0);
    }
    public void vibrate_kapa()
    {
        PlayerPrefs.SetInt("vibrate", 1);
    }
}
