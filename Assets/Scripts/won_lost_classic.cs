﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class won_lost_classic : MonoBehaviour
{
    public int selectedflag, zamani = 200;
    bool kazandinbool;
    public Image Flag;
    public Sprite[] countries;
    public GameObject karakter;
    public AudioSource sesoynat;

    void Start()
    {
        sesoynat.Play();
        if (karakter.GetComponent<Karakter>().yedigim >= 3)
        {
            karakter.GetComponent<Karakter>().failses.Play();
        }
        if (karakter.GetComponent<Karakter>().attigim >= 3)
        {
            int kazan = PlayerPrefs.GetInt("kazandin1");
            kazan += 1;
            PlayerPrefs.SetInt("kazandin1", kazan);
            karakter.GetComponent<Karakter>().wonses.Play();
        }
        selectedflag = PlayerPrefs.GetInt("flag");
        Flag.sprite = countries[selectedflag];

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        zamani--;
        if (zamani < 0)
        {
            PhotonNetwork.LeaveRoom();
            PhotonNetwork.Disconnect();
            Application.LoadLevel("MainMenu");
        }
    }
    public void paraal()
    {
        if(kazandinbool == false)
        {
            float money = PlayerPrefs.GetFloat("money");
            money += 250;
            PlayerPrefs.SetFloat("money", money);
            kazandinbool = true;
        }
    }
}
