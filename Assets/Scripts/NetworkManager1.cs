﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client;

public class NetworkManager1 : MonoBehaviourPunCallbacks
{


    public GameObject EnterGamePanel;
    public GameObject ConnectionStatusPanel;
    public GameObject LobbyPanel;
    public GameObject FriendPanel;
    public bool isFriend, isCreated, isPublic;
    public string modesecimstr;
    public string roomcode;
    public byte maxplyr;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        isPublic = true;
        EnterGamePanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);

    }
    public void playwfriendloginback()
    {
        EnterGamePanel.gameObject.SetActive(true);
        FriendPanel.gameObject.SetActive(false);

    }
    void Update()
    {
        if (PhotonNetwork.CountOfPlayers >= 20)
        {
            Application.LoadLevel("MainMenu");
            PlayerPrefs.SetInt("doluluk", 1);
        }
        if(FriendPanel.activeInHierarchy == true)
        {
            roomcode = GameObject.Find("TextJoin").GetComponent<Text>().text;
        }


    }
    public void ConnectToPhotonServer()
    {
        if (isFriend)
        {
            roomcode = GameObject.Find("TextJoin").GetComponent<Text>().text;

        }

        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            FriendPanel.SetActive(false);
            EnterGamePanel.SetActive(false);
            ConnectionStatusPanel.SetActive(true);

        }



    }
    public void JoinRandomRoom()
    {
        if (isFriend == false)
        {
            string[] roompropsinlobby = { "gm" };
            ExitGames.Client.Photon.Hashtable myhastable = new ExitGames.Client.Photon.Hashtable() { { "gm", modesecimstr } };
            PhotonNetwork.JoinRandomRoom(myhastable, 0);
        }
        else if (isFriend == true)
        {
            if (isCreated)
            {
                CreateAndJoinRoom();
            }
            else if (isCreated == false)
            {
                PhotonNetwork.JoinRoom("Room" + roomcode);

            }
        }
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.NickName + "Connected");
        LobbyPanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
    }

    public override void OnConnected()
    {
        Debug.Log("Connected To Internet");
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log(message);
        CreateAndJoinRoom();
    }
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        CreateAndJoinRoom();
    }
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.NickName + "  joined to  " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel(modesecimstr);
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.NickName + "  joined to  " + PhotonNetwork.CurrentRoom.Name + "  " + PhotonNetwork.CurrentRoom.PlayerCount);
    }
    public void playwfriendlogin()
    {
        EnterGamePanel.gameObject.SetActive(false);
        FriendPanel.gameObject.SetActive(true);

    }
    public void playwfriendcreate()
    {
        isFriend = true;
        isCreated = true;
    }
    public void playwfriendjoint()
    {
        isFriend = true;
        isCreated = false;
    }
    #region private methods
    void CreateAndJoinRoom()
    {
        string randomroomname = "Room" + Random.Range(301, 601);
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.MaxPlayers = maxplyr;
        roomOptions.IsVisible = !isFriend;
        string[] roompropsinlobby = { "gm" };
        ExitGames.Client.Photon.Hashtable myhastable = new ExitGames.Client.Photon.Hashtable() { { "gm", modesecimstr } };
        roomOptions.CustomRoomPropertiesForLobby = roompropsinlobby;
        roomOptions.CustomRoomProperties = myhastable;

        PhotonNetwork.CreateRoom(randomroomname, roomOptions);
    }

    #endregion

}
