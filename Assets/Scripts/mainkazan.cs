﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mainkazan : MonoBehaviour
{
    public Text won, lost, all, percent, allc, wonc, lostc, percentc , report;
    public GameObject faq, doluluk,choose, connection, settings;
    public int dolulukint, internetint;
    public string disconnectstr;

    void Start()
    {
        disconnectstr = PlayerPrefs.GetString("disconnectreason");
        if(disconnectstr != "")
        {
            report.text = disconnectstr;
            disconnectstr = "";
            PlayerPrefs.SetString("disconnectreason", disconnectstr);
        }
        if (PlayerPrefs.GetInt("choosed") == 0)
        {
            choose.SetActive(true);
        }
        dolulukint = PlayerPrefs.GetInt("doluluk");
        internetint = PlayerPrefs.GetInt("connectionfail");
        if(dolulukint == 1)
        {
            doluluk.SetActive(true);
            PlayerPrefs.SetInt("doluluk", 0);
        }

    }

    // Update is called once per frame
    void Update()
    {
        all.text = PlayerPrefs.GetInt("girdigim").ToString();
        won.text = PlayerPrefs.GetInt("kazandin").ToString();
        lost.text = (PlayerPrefs.GetInt("girdigim") - PlayerPrefs.GetInt("kazandin")).ToString();
        if (PlayerPrefs.GetInt("girdigim") != 0)
        {
            percent.text = "% " + (100*PlayerPrefs.GetInt("kazandin") / PlayerPrefs.GetInt("girdigim")).ToString();
        }
        allc.text = PlayerPrefs.GetInt("girdigim1").ToString();
        wonc.text = PlayerPrefs.GetInt("kazandin1").ToString();
        lostc.text = (PlayerPrefs.GetInt("girdigim1") - PlayerPrefs.GetInt("kazandin1")).ToString();
        if (PlayerPrefs.GetInt("girdigim1") != 0)
        {
            percentc.text = "% " + (100*PlayerPrefs.GetInt("kazandin1") / PlayerPrefs.GetInt("girdigim1")).ToString();
        }



    }
    public void imgackapa(bool acikmi)
    {
        faq.gameObject.SetActive(acikmi);
    }
    public void img2ackapa(bool acikmi)
    {
        doluluk.gameObject.SetActive(acikmi);

    }
    public void img3ackapa(bool acikmi)
    {
        PlayerPrefs.SetInt("connectionfail", 0);
        connection.gameObject.SetActive(acikmi);

    }
    public void img4ackapa(bool acikmi)
    {
        settings.gameObject.SetActive(acikmi);

    }
}
