﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class single_kontrol : MonoBehaviour
{
    public float hiz;
    public bool ileribool, sagbool, solbool, ilerigeri;
    public GameObject ilerivites, gerivites;
    public int skin;

    void Start()
    {
        skin = PlayerPrefs.GetInt("skin");
        ilerigeri = true;
        if (skin == 0)
        {
            hiz = 0.25f;
        }
        if (skin == 1)
        {
            hiz = 0.25f;
        }
        if (skin == 2)
        {
            hiz = 0.27f;
        }
        if (skin == 3)
        {
            hiz = 0.3f;
        }
        if (skin == 4)
        {
            hiz = 0.3f;
        }
        if (skin == 5)
        {
            hiz = 0.32f;
        }
        if (skin == 6)
        {
            hiz = 0.32f;
        }
        if (skin == 7)
        {
            hiz = 0.32f;
        }
        if (skin == 8)
        {
            hiz = 0.35f;
        }
        if (skin == 9)
        {
            hiz = 0.35f;
        }
        hiz = hiz * 2;
    }

    void Update()
    {

        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2(0, moveVertical);
        this.transform.Translate(movement * (hiz));

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, 0, 1.5f);

        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 0, -1.5f);

        }

        if (ileribool)
        {
            if (ilerigeri == true)
            {
                transform.Translate(0, hiz, 0);

            }
            else
            {
                transform.Translate(0, -hiz / 1.5f, 0);

            }
        }
        if (sagbool)
        {
            transform.Rotate(0, 0, -hiz*5);
        }
        if (solbool)
        {
            transform.Rotate(0, 0, hiz*5);
        }


    }
    public void ileri()
    {
        ileribool = true;
    }
    public void ileribirak()
    {
        ileribool = false;
    }
    public void solbirak()
    {
        solbool = false;
    }
    public void sagbirak()
    {
        sagbool = false;
    }

    public void donmesag()
    {
        sagbool = true;
    }
    public void donmesol()
    {
        solbool = true;
    }
    public void vitesayarla()
    {
        if (ilerigeri == true)
        {
            gerivites.gameObject.SetActive(true);
            ilerivites.gameObject.SetActive(false);
            ilerigeri = false;
        }
        else if (ilerigeri == false)
        {
            gerivites.gameObject.SetActive(false);
            ilerivites.gameObject.SetActive(true);
            ilerigeri = true;
        }
    }
}
