﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAP : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void satinalindi1000gem()
    {
        int gem = PlayerPrefs.GetInt("gem");
        gem += 1000;
        PlayerPrefs.SetInt("gem", gem);
    }
    public void satinalindi2000gem()
    {
        int gem = PlayerPrefs.GetInt("gem");
        gem += 2000;
        PlayerPrefs.SetInt("gem", gem);
    }
    public void satinalindi3000gem()
    {
        int gem = PlayerPrefs.GetInt("gem");
        gem += 3000;
        PlayerPrefs.SetInt("gem", gem);
    }
    public void satinalindi10_000gem()
    {
        int gem = PlayerPrefs.GetInt("gem");
        gem += 10000;
        PlayerPrefs.SetInt("gem", gem);
    }
    public void satinalindi50_000coin()
    {
        float money = PlayerPrefs.GetFloat("money");
        money += 50000;
        PlayerPrefs.SetFloat("money", money);
    }
    public void satinalindi20_000coin()
    {
        float money = PlayerPrefs.GetFloat("money");
        money += 20000;
        PlayerPrefs.SetFloat("money", money);
    }
    public void satinalindi10_000coin()
    {
        float money = PlayerPrefs.GetFloat("money");
        money += 10000;
        PlayerPrefs.SetFloat("money", money);
    }
    public void satinalindi5_000coin()
    {
        float money = PlayerPrefs.GetFloat("money");
        money += 5000;
        PlayerPrefs.SetFloat("money", money);
    }
}
