﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

public class fut_Karakter : MonoBehaviourPunCallbacks
{
    public int girdim, pingint = 100,mermim,a;
    public float can;
    public string plyr;
    public GameObject playernamego, playerpatlama, kale, yendinpanel, yenildinpanel,waitpanel, mytop, mytopdiger, top, player1has,player2has;
    public Text durumtxt,p1txt,p2txt,yendintxt;
    public float yedigim, attigim;
    public bool mermibl;
    public Text pingtxt;

    void Start()
    {
        if (photonView.IsMine)
        {
            top = GameObject.Find("topoyun");
            girdim = PlayerPrefs.GetInt("girdigim");
            girdim += 1;
            PlayerPrefs.SetInt("girdigim", girdim);
        }
        this.transform.localEulerAngles = new Vector3(0, 0, -90);

    }
   
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        durumtxt.text = "YOU          " + "WON";
        yendinpanel.gameObject.SetActive(true);
        this.GetComponent<fut_Kontrol>().enabled = false;
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponent<BoxCollider2D>().enabled = false;
        mytop.gameObject.SetActive(false);
        playerpatlama.gameObject.SetActive(true);
        playerpatlama.GetComponent<Animator>().SetBool("expanimbool", true);
        yendinpanel.GetComponent<won_lost>().kazandimmi = true;
    }

    void FixedUpdate()
    {

        pingint--;
        if (pingint <= 0)
        {
            pingtxt.text = "ping: " + PhotonNetwork.GetPing().ToString() + " ms";
            pingint = 100;
        }

        top = GameObject.Find("top");
        kale = GameObject.Find("kale");
        if (this.gameObject.name == "Player1")
        {
            attigim = kale.GetComponent<fut_kale>().p1gol;
            yedigim = kale.GetComponent<fut_kale>().p2gol;
            p1txt.text = attigim.ToString();
            p2txt.text = yedigim.ToString();
          /*  if (top.GetComponent<top>().annenbl == false)
            {
                if (mermim == 1)
                {
                    player1has.gameObject.SetActive(true);
                    player2has.gameObject.SetActive(false);

                }
                if (mermim == 0)
                {
                    player2has.gameObject.SetActive(true);
                    player1has.gameObject.SetActive(false);

                }
            }
            else if (top.GetComponent<top>().annenbl == true)
            {

                player1has.gameObject.SetActive(false);
                player2has.gameObject.SetActive(false);

            }*/

        }
        if (this.gameObject.name == "Player2")
        {
            attigim = kale.GetComponent<fut_kale>().p2gol;
            yedigim = kale.GetComponent<fut_kale>().p1gol;
            p2txt.text = attigim.ToString();
            p1txt.text = yedigim.ToString();
          /*  if (top.GetComponent<top>().annenbl == false)
            {
                if (mermim == 1)
                {
                    player2has.gameObject.SetActive(true);
                    player1has.gameObject.SetActive(false);

                }
                if (mermim == 0)
                {
                    player1has.gameObject.SetActive(true);
                    player2has.gameObject.SetActive(false);

                }
            }
            else if (top.GetComponent<top>().annenbl == true)
            {

                player1has.gameObject.SetActive(false);
                player2has.gameObject.SetActive(false);

            }
            */
        }

        if(yedigim == 3)
        {
            durumtxt.text = "YOU          " + "DIED";
            yenildinpanel.gameObject.SetActive(true);
            this.GetComponent<fut_Kontrol>().enabled = false;
            this.GetComponent<SpriteRenderer>().enabled = false;
            this.GetComponent<BoxCollider2D>().enabled = false;
            mytop.gameObject.SetActive(false);
            playerpatlama.gameObject.SetActive(true);
            playerpatlama.GetComponent<Animator>().SetBool("expanimbool", true);
           

        }
        if (attigim == 3)
        {
            durumtxt.text = "YOU          " + "WON";
            yendinpanel.gameObject.SetActive(true);
            yendinpanel.GetComponent<won_lost>().kazandimmi = true;
            this.GetComponent<fut_Kontrol>().enabled = false;
            this.GetComponent<SpriteRenderer>().enabled = false;
            this.GetComponent<BoxCollider2D>().enabled = false;
            mytop.gameObject.SetActive(false);
            playerpatlama.gameObject.SetActive(true);
            playerpatlama.GetComponent<Animator>().SetBool("expanimbool", true);
           
        }
        if (mermim == 0)
        {
            if (mermibl)
            {
                a--;
                if (a <= 0)
                {
                    mytop.GetComponent<Animator>().SetBool("animbool", false);
                    this.GetComponent<PhotonView>().RPC("mermibende", RpcTarget.All, false);
                    mermibl = false;
                }

            }
        }
        if (mermim > 1)
        {
            mermim = 1;
        }
        if (mermim > 0)
        {
            mermibl = true;
        }

        if (photonView.IsMine)
        {
            playernamego.GetComponent<TextMesh>().text = "Player" + PhotonNetwork.LocalPlayer.ActorNumber.ToString();
            PhotonNetwork.LocalPlayer.NickName = "Player" + PhotonNetwork.LocalPlayer.ActorNumber.ToString();

        }

        plyr = PhotonNetwork.LocalPlayer.ActorNumber.ToString();

    }
    [PunRPC]
    public void topackapa(bool acik)
    {
        top.GetComponent<SpriteRenderer>().enabled = acik;
        top.GetComponent<CircleCollider2D>().enabled = acik;
    }
    [PunRPC]
    public void mermibende(bool aqq)
    {
        mytopdiger.gameObject.SetActive(aqq);

    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "topoyun")
        {
            this.GetComponent<PhotonView>().RPC("mermibende", RpcTarget.All, true);
            mytop.gameObject.SetActive(true);
            mermim = 1;
            a = 50;

        }

        }
    }





