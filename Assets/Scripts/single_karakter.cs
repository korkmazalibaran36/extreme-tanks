﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class single_karakter : MonoBehaviour
{

    public int girdim;
    public float can, zaman, attigim;
    public GameObject playerpatlama, kale, yendinpanel, mytop, topalinan;
    public Text  yendintxt, p1txt,zamantxt;
    public int  mermim, a;
    public bool mermibl;
    public AudioSource asmy;


    void Start()
    {

        this.transform.localEulerAngles = new Vector3(0, 0, -90);

    }

    

    void FixedUpdate()
    {
        zaman--;
        zamantxt.text = (zaman / 60).ToString();
        kale = GameObject.Find("kale");
        if (this.gameObject.name == "Player1")
        {

            p1txt.text = attigim.ToString();

        }
        
      

        
        if (zaman <= 0)
        {
            asmy.Play();
            zaman = 0;
            yendinpanel.GetComponent<singlewon>().kazandimmi = true;
            yendinpanel.SetActive(true);
            this.GetComponent<single_kontrol>().enabled = false;
            this.GetComponent<SpriteRenderer>().enabled = false;
            this.GetComponent<BoxCollider2D>().enabled = false;
            mytop.gameObject.SetActive(false);
            playerpatlama.gameObject.SetActive(true);
            playerpatlama.GetComponent<Animator>().SetBool("expanimbool", true);

        }
        if (mermim == 0)
        {
            if (mermibl)
            {
                a--;
                if (a == 0)
                {
                    mytop.GetComponent<Animator>().SetBool("animbool", false);
                    mermibl = false;
                }
            }
        }
        if (mermim > 1)
        {
            mermim = 1;
        }
        if (mermim > 0)
        {
            mermibl = true;
        }
       


    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "top")
        {
            mermim += 1;
            a = 30;
            mytop.gameObject.SetActive(true);
            topalinan = other.gameObject;
            other.gameObject.SetActive(false);
        }

    }
}
