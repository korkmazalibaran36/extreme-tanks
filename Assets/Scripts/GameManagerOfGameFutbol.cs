﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class GameManagerOfGameFutbol : MonoBehaviourPunCallbacks
{

    [SerializeField]
    GameObject playerPrefab, top;

    



    void Start()
    {
        if (PhotonNetwork.IsConnected)
        {
            if (playerPrefab != null)
            {
                if(PhotonNetwork.LocalPlayer.ActorNumber == 1)
                {
                    PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(-58, 4.5f, 0), Quaternion.identity);

                }
                if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                {
                    PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(-58, -2.5f, 0), Quaternion.identity);

                }
            }

        }
    }

    // Update is called once per frame
    void Update()
    {

        
    }
    public override void OnJoinedRoom()
    {
        Debug.Log("Player" + PhotonNetwork.LocalPlayer.ActorNumber.ToString() + "  joined to  " + PhotonNetwork.CurrentRoom.Name);

    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("Player" + PhotonNetwork.LocalPlayer.ActorNumber.ToString() + "  joined to  " + PhotonNetwork.CurrentRoom.Name + " " + PhotonNetwork.CurrentRoom.PlayerCount + " player(s) activated. ");

    }
   
}
