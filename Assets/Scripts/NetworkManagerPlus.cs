﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class NetworkManagerPlus : MonoBehaviourPunCallbacks
{

    public GameObject EnterGamePanel;
    public GameObject ConnectionStatusPanel;
    public GameObject LobbyPanel;
    public GameObject FriendPanel;
    public GameObject togglew;
    public bool isFriend, isCreated, isPublic;
    public string modesecimstr;
    public string roomcode, gamemap;
    public byte maxplyr;
    public Toggle stadiumtog, roadtog;
    public Text bolgetxt;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        isPublic = true;
        EnterGamePanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);

    }

    void Update()
    {
        bolgetxt.text = PhotonNetwork.CloudRegion;
        if(PhotonNetwork.CountOfPlayers >= 20)
        {
            Application.LoadLevel("MainMenu");
            PlayerPrefs.SetInt("doluluk", 1);
        }
        if(stadiumtog.isOn == true)
        {
            roadtog.isOn = false;
            modesecimstr = "GameScenePlus";
            gamemap = "GameScenePlus";
        }
        if (roadtog.isOn == true)
        {
            stadiumtog.isOn = false;
            gamemap = "GameSceneRoadPlus";
            modesecimstr = "GameSceneRoadPlus";

        }


    }
    public void ConnectToPhotonServer()
    {
        if (isFriend)
        {
            roomcode = GameObject.Find("TextJoin").GetComponent<Text>().text;

        }

        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            FriendPanel.SetActive(false);
            EnterGamePanel.SetActive(false);
            ConnectionStatusPanel.SetActive(true);

        }
       

    }
    public void JoinRandomRoom()
    {
        if (isFriend == false)
        {
            string[] roompropsinlobby = { "gm" };
            ExitGames.Client.Photon.Hashtable myhastable = new ExitGames.Client.Photon.Hashtable() { { "gm", modesecimstr } };
            PhotonNetwork.JoinRandomRoom(myhastable,0);
        }
        else if(isFriend == true)
        {
            if (isCreated)
            {
                CreateAndJoinRoom();
            }
            else if(isCreated == false)
            {
                PhotonNetwork.JoinRoom("Room" + roomcode);

            }
        }
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.NickName + "Connected");
        LobbyPanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
    }

    public override void OnConnected()
    {
        Debug.Log("Connected To Internet");
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log(message);
        CreateAndJoinRoom();
    }
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        // CreateAndJoinRoom();
        PhotonNetwork.CreateRoom("Room" + roomcode, new RoomOptions { MaxPlayers = maxplyr });

    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        PhotonNetwork.JoinRoom("Room" + roomcode);
    }
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.NickName + "  joined to  " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel(modesecimstr);
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.NickName + "  joined to  " + PhotonNetwork.CurrentRoom.Name + "  " + PhotonNetwork.CurrentRoom.PlayerCount);
    }
    public void playwfriendlogin()
    {
        EnterGamePanel.gameObject.SetActive(false);
        FriendPanel.gameObject.SetActive(true);

    }
    public void playwfriendloginback()
    {
        EnterGamePanel.gameObject.SetActive(true);
        FriendPanel.gameObject.SetActive(false);

    }
    public void playwfriendcreate()
    {
        isFriend = true;
        isCreated = true;
        isPublic = togglew.GetComponent<Toggle>().isOn;
    }
    public void playwfriendjoint()
    {
        isFriend = true;
        isCreated = false;
    }
    #region private methods
    void CreateAndJoinRoom()
    {
        if (modesecimstr != null)
        {
            string randomroomname = "Room" + Random.Range(602, 1500);
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.IsOpen = true;
            roomOptions.MaxPlayers = maxplyr;
            roomOptions.IsVisible = isPublic;
            roomOptions.PlayerTtl = 0;
            string[] roompropsinlobby = { "gm" };
            ExitGames.Client.Photon.Hashtable myhastable = new ExitGames.Client.Photon.Hashtable() { {"gm", modesecimstr } };
            roomOptions.CustomRoomPropertiesForLobby = roompropsinlobby;
            roomOptions.CustomRoomProperties = myhastable;

            PhotonNetwork.CreateRoom(randomroomname, roomOptions);
        }
    }

    #endregion

}
