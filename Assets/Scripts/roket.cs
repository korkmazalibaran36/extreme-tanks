﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class roket : MonoBehaviourPunCallbacks
{
    public bool  vur, rakipoldu, vurkontrol;
    public int  a, sarjor,skin,inta,fx;
    public Animator rokanim;
    public float hasar, para,para_a, hasarkontrol;
    public GameObject mykarakter,butonvurus,duman, effect, rakibim, vurulantasfx;
    public Text sarjortxt,paratxt,durumtxt;
    public AudioSource youhaveshoot, ukilled;
    public AudioSource nobullet, boughtbullet;
    void Start()
    {
        fx = PlayerPrefs.GetInt("fx");
        a = 0;
        skin = PlayerPrefs.GetInt("skin");
        if (skin == 0)
        {
            sarjor = 10;
            hasar = 10;
        }
        if (skin == 1)
        {
            sarjor = 10;
            hasar = 15;
        }
        if (skin == 2)
        {
            sarjor = 11;
            hasar = 15;
        }
        if (skin == 3)
        {
            sarjor = 12;
            hasar = 20;
        }
        if (skin == 4)
        {
            sarjor = 12;
            hasar = 25;
        }
        if (skin == 5)
        {
            sarjor = 13;
            hasar = 25;
        }
        if (skin == 6)
        {
            sarjor = 14;
            hasar = 25;
        }
        if (skin == 7)
        {
            sarjor = 14;
            hasar = 30;
        }
        if (skin == 8)
        {
            hasar = 30;
            sarjor = 15; 
        }
        if (skin == 9)
        {
            sarjor = 15;
            hasar = 30;
        }

        if (photonView.IsMine)
        {
            if (fx == 2)
            {
                photonView.RPC("hasarayarla", RpcTarget.AllBuffered, 5);
            }
            if (fx == 4)
            {
                sarjor += 4;
            }
            if (fx == 6)
            {
                sarjor += 7;
            }
            if (fx == 7)
            {
                sarjor += 5;
            }
            if (fx == 5)
            {
                photonView.RPC("hasarayarla", RpcTarget.AllBuffered, 10);
            }
            if (fx == 8)
            {
                photonView.RPC("hasarayarla", RpcTarget.AllBuffered, 5);
            }
            if (fx == 9)
            {
                photonView.RPC("hasarayarla", RpcTarget.AllBuffered, 10);
                sarjor += 10;
            }
        }
        hasarkontrol = hasar;

    }

    [PunRPC]
    public void hasarayarla(int iqq)
    {
        this.GetComponent<roket>().hasar += iqq;
    }
    public void quitt()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
        Application.LoadLevel("MainMenu");
    }
    public void FixedUpdate()
    {

        if (vur)
        {
            butonvurus.GetComponent<Button>().interactable = false;
            a--;
            if (a == 0)
            {
                rokanim.SetBool("animbool", false);
            }
            if (a < 0)
            {
                butonvurus.GetComponent<Button>().interactable = true;

            }
        }
        para_a = hasar * 1.5f;
        paratxt.text = para.ToString();
        para = PlayerPrefs.GetFloat("money");
        sarjortxt.text = sarjor.ToString();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            vurhedef();
            durumtxt.text = " ";
        }
        if (rakipoldu)
        {
            inta--;
            if(inta == 0)
            {
                this.GetComponent<PolygonCollider2D>().enabled = true;
                rakipoldu = false;

            }
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            sarjoralma();
        }


    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name != PhotonNetwork.LocalPlayer.NickName & other.gameObject.CompareTag("Player") && mykarakter.GetComponent<Karakter>().partnerim != other.gameObject)
        {
            rokanim.SetBool("animbool", false);
            rokanim.Play("New State");
            rakibim = other.gameObject;
            para += para_a;
            PlayerPrefs.SetFloat("money", para);
            durumtxt.text = "You Have Shoot An Enemy!" + " +" + "$$$";
            youhaveshoot.Play();
            Debug.Log("değdi");
            mykarakter.GetComponent<AudioSource>().Play();
            other.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, hasar, true);
            Debug.Log(other.GetComponent<Karakter>().can);
            if (other.gameObject.tag != "roket")
            {
                if (other.GetComponent<Karakter>().can <= 0)
                {
                    //GameObject effectDefGo = PhotonNetwork.Instantiate(effect.name, other.transform.position, Quaternion.LookRotation(other.transform.localEulerAngles), 0);
                    //effectDefGo.GetComponent<ParticleSystem>().Play();
                    rakipoldu = true;
                    ukilled.Play();
                    mykarakter.GetComponent<Karakter>().Respawn();
                    inta = 70;
                    this.GetComponent<PolygonCollider2D>().enabled = false;
                    if (this.name == "1")
                    {
                        mykarakter.gameObject.GetComponent<scoreclassic>().p1scorearttir(1);
                    }
                    if (this.name == "2")
                    {
                        mykarakter.gameObject.GetComponent<scoreclassic>().p2scorearttir(1);
                    }
                }
            }

        }
        if(other.gameObject.tag == "rock")
        {
            rokanim.SetBool("animbool", false);
            rokanim.Play("New State");
            mykarakter.GetComponent<AudioSource>().Play();
            GameObject.Instantiate(vurulantasfx, new Vector3(other.transform.position.x, other.transform.position.y, 0), Quaternion.identity);
        }

        /* else if (other.gameObject.tag == "Roket")
         {
             mykarakter.GetComponent<Karakter>().can += 5;
             durumtxt.text = "";
             youhaveshoot.Stop();
             duman.GetComponent<duman>().enemyshooted.Stop();

         }*/


    }

    public void vurhedef()
    {

        if (sarjor > 0)
        {
            if (a <= 0)
            {
                sarjor -= 1;
                Debug.Log(this.name);
                a = 30;
                rokanim.SetBool("animbool", true);
                vur = true;
            }

        }
        else
        {
            nobullet.Play();
        }

    }

    public void sarjoralma()
    {
        if (para >= 100)
        {
            sarjor += 10;
            para -= 100;
            PlayerPrefs.SetFloat("money", para);
            boughtbullet.Play();
            durumtxt.text = "You Bought Bullets!";

        }
        else
        {
            durumtxt.text = "You Don't Have Enough Money";
            
        }
    }
    /*
    public void x2_aktif()
    {

    }
    [PunRPC]
    public void upgrade()
    {

    }*/
    [PunRPC]
    public void ismim(string a)
    {
        this.name = a;
    }
}
