﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class fut_playersetup : MonoBehaviourPunCallbacks
{

    public GameObject tank, roket, canvasgo;

    void Start()
    {

        if (photonView.IsMine)
        {
            tank.GetComponent<fut_Karakter>().enabled = true;
            tank.GetComponent<fut_Kontrol>().enabled = true;
            tank.GetComponent<fut_Kontrol>().maincam.enabled = true;
            canvasgo.SetActive(true);
            roket.GetComponent<fut_roket>().enabled = true;


        }
        else
        {
            tank.GetComponent<fut_Kontrol>().enabled = false;
            tank.GetComponent<fut_Karakter>().enabled = false;
            tank.GetComponent<fut_Kontrol>().maincam.enabled = false;
            canvasgo.SetActive(false);
            roket.GetComponent<fut_roket>().enabled = false;


        }

    }

    void Update()
    {

    }
}
