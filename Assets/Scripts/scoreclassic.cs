﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreclassic : MonoBehaviourPunCallbacks
{
    public int p1score, p2score, ai = 200;
    public Text p1text, p2text;
    bool bl;

    void Start()
    {

    }

    void FixedUpdate()
    {
        p1text.text = p1score.ToString();
        p2text.text = p2score.ToString();
        if (bl)
        {
            ai--;
            if (ai <= 0)
            {
                bl = false;
            }
        }


    }
    public void p1scorearttir(int a)
    {
        if (bl == false)
        {
            p1score += a;
            bl = true;
            ai = 200;
        }
    }
    public void p2scorearttir(int a)
    {
        if (bl == false)
        {
            p2score += a;
            bl = true;
            ai = 200;
        }
    }
}


