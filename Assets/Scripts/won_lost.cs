﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class won_lost : MonoBehaviourPunCallbacks
{
    public int canbitti ;
    public float para, kazandigimpara;
    public GameObject mykarakter;
    public bool kazandimmi;
    public Text kazandintxt;

    void Start()
    {
        if (kazandimmi == true)
        {
            int kazan =  PlayerPrefs.GetInt("kazandin");
            para = PlayerPrefs.GetFloat("money");
            kazandigimpara = (mykarakter.GetComponent<fut_Karakter>().attigim) * 150;
            if (kazandigimpara > 0)
            {
                kazan += 1;
                PlayerPrefs.SetInt("kazandin", kazan);
                para += kazandigimpara;
                PlayerPrefs.SetFloat("money", para);
                kazandintxt.text = "+" + kazandigimpara.ToString();
            }
            
        }
        if(PhotonNetwork.LocalPlayer.ActorNumber == 2)
        {
            canbitti = 100;

        }
        if(PhotonNetwork.LocalPlayer.ActorNumber == 1)
        {
            canbitti = 100;

        }
    }

    void FixedUpdate()
    {
        canbitti--;


        if (canbitti == 0)
        {
            PhotonNetwork.LeaveRoom();
            PhotonNetwork.Disconnect();
            Application.LoadLevel("MainMenu");

        }
  

    }
}
