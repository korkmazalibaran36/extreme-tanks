﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shop : MonoBehaviour
{
    public int kontroli, gem, fiyat;
    public float para;
    public GameObject acilacak, kapanacak;
    public new string name;

    void Update()
    {
        kontroli = PlayerPrefs.GetInt(name);
        if (kontroli == 1)
        {

            acilacak.gameObject.SetActive(true);
            kapanacak.gameObject.SetActive(false);

        }
        para = PlayerPrefs.GetFloat("money");
        gem = PlayerPrefs.GetInt("gem");
        Debug.Log(para);
    }


    public void satinalim()
    {
        if (para >= fiyat)
        {
            PlayerPrefs.SetInt(name, 1);
            para -= fiyat;
            PlayerPrefs.SetFloat("money", para);



        }
    }
    public void satinalimgem()
    {
        if (gem >= fiyat)
        {
            PlayerPrefs.SetInt(name, 1);
            gem -= fiyat;
            PlayerPrefs.SetInt("gem", gem);



        }
    }
}
