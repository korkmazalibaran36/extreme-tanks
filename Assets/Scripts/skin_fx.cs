﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skin_fx : MonoBehaviourPunCallbacks
{
    public GameObject[] myfx;
    public int fxno;

    void Start()
    {
        fxno = PlayerPrefs.GetInt("fx");

            if (photonView.IsMine)
            {
                photonView.RPC("fx_active", RpcTarget.AllBuffered, fxno, true);
            }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    [PunRPC]
    public void fx_active(int skincek, bool annen)
    {
        this.GetComponent<skin_fx>().myfx[skincek].SetActive(annen);

    }
}
