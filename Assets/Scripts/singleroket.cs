﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class singleroket : MonoBehaviour
{
    public bool vur;
    public int a = 30, sarjor, vibri;
    public Animator rokanim;
    public float hasar, para, para_a;
    public GameObject mykarakter, butonvurus,camera;
    public Text sarjortxt, paratxt, durumtxt;
    public AudioSource ahh, goall;

    void Start()
    {
        sarjor = 10;
        vibri = PlayerPrefs.GetInt("vibrate");

    }
    public void FixedUpdate()
    {
        sarjor = mykarakter.GetComponent<single_karakter>().mermim;
        paratxt.text = para.ToString();
        para = PlayerPrefs.GetFloat("money");
        sarjortxt.text = sarjor.ToString();
        if (vur)
        {
            butonvurus.GetComponent<Button>().interactable = false;
            a--;
            if (a == 0)
            {
                rokanim.SetBool("animbool", false);

            }
            if (a < 0)
            {
                butonvurus.GetComponent<Button>().interactable = true;

            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            vurhedef();
        }


    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "kale")
        {
            if (this.gameObject.name == "1")
            {
                camera.GetComponent<Animator>().Play("camvibration");
                if (vibri == 0)
                {
                   Handheld.Vibrate();
                }
                goall.Play();
                para += para_a;
                mykarakter.GetComponent<single_karakter>().attigim += 1;
                PlayerPrefs.SetFloat("money", para);
                Debug.Log("değdi");
                rokanim.SetBool("animbool", false);
                rokanim.Play("New State");

            }

        }
        if (other.gameObject.name == "direk")
        {
            ahh.Play();
            rokanim.SetBool("animbool", false);
            rokanim.Play("New State");

        }
        if (other.gameObject.tag == "Player")
        {
            if (other.gameObject.name == "Player")
                ahh.Play();
            rokanim.SetBool("animbool", false);
            rokanim.Play("New State");

        }


    }
    public void vurhedef()
    {
        if (sarjor > 0)
        {
            mykarakter.GetComponent<single_karakter>().mermim -= 1;
            mykarakter.GetComponent<single_karakter>().topalinan.SetActive(true);
            Debug.Log(this.name);
            rokanim.SetBool("animbool", true);
            a = 30;
            vur = true;
        }
    }
    public void sarjoralma()
    {

    }
}
