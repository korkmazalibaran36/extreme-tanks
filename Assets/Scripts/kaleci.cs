﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kaleci : MonoBehaviour
{
    public PhotonView photonView;
    public SpriteRenderer myspriter;
    public PolygonCollider2D mycollider;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        myspriter = this.GetComponent<SpriteRenderer>();
        mycollider = this.GetComponent<PolygonCollider2D>();
    }
    [PunRPC]
    public void superguc(bool acikmi)
    {
        myspriter.enabled = acikmi;
        mycollider.enabled = acikmi;
    }
}
