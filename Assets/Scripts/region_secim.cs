﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class region_secim : MonoBehaviour
{
    public string a;
    public Text regiontext;
    void Start()
    {
        
    }

    void Update()
    {
        a = PlayerPrefs.GetString("region");
        if(a == "")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = null;
            regiontext.text = "BEST REGION ✓";

        }
        else if (a == "asia")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "asia";
            regiontext.text = "ASIA";

        }
        else if (a == "eu")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "eu";
            regiontext.text = "EUROPA";

        }
        else if (a == "au")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "au";
            regiontext.text = "AUSTRALIA";

        }
        else if (a == "us")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "us";
            regiontext.text = "USA";

        }
        else if (a == "usw")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "usw";
            regiontext.text = "USA WEST";

        }
        else if (a == "cn")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "cn";
            regiontext.text = "CHINA";

        }
        else if (a == "cae")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "cae";
            regiontext.text = "CANADA EAST";
        }
        else if (a == "jp")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "jp";
            regiontext.text = "JAPAN";

        }
        else if (a == "ru")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "ru";
            regiontext.text = "RUSSIA";

        }
        else if (a == "rue")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "rue";
            regiontext.text = "RUSSIA EAST";

        }
        else if (a == "in")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "in";
            regiontext.text = "INDIA";

        }
        else if (a == "sa")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "sa";
            regiontext.text = "SOUTH AMERICA";

        }
        else if (a == "za")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "za";
            regiontext.text = "SOUTH AFRICA";

        }
        else if (a == "kr")
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "kr";
            regiontext.text = "KOREA";

        }
    }

    public void regionsec()
    {
        string regionstr = a;
        if (regionstr == "")
        {
            PlayerPrefs.SetString("region", "asia");
        }
        if (regionstr == "asia")
        {
            PlayerPrefs.SetString("region", "eu");
        }
        if (regionstr == "eu")
        {
            PlayerPrefs.SetString("region", "au");
        }
        if (regionstr == "au")
        {
            PlayerPrefs.SetString("region", "us");
        }
        if (regionstr == "us")
        {
            PlayerPrefs.SetString("region", "usw");
        }
        if (regionstr == "usw")
        {
            PlayerPrefs.SetString("region", "cn");
        }
        if (regionstr == "cn")
        {
            PlayerPrefs.SetString("region", "cae");
        }
        if (regionstr == "cae")
        {
            PlayerPrefs.SetString("region", "jp");
        }
        if (regionstr == "jp")
        {
            PlayerPrefs.SetString("region", "ru");
        }
        if (regionstr == "ru")
        {
            PlayerPrefs.SetString("region", "rue");
        }
        if (regionstr == "rue")
        {
            PlayerPrefs.SetString("region", "in");
        }
        if (regionstr == "in")
        {
            PlayerPrefs.SetString("region", "sa");
        }
        if (regionstr == "sa")
        {
            PlayerPrefs.SetString("region", "kr");
        }
        if (regionstr == "kr")
        {
            PlayerPrefs.SetString("region", "za");
        }
        if (regionstr == "za")
        {
            PlayerPrefs.SetString("region", "");
        }

    }
}
