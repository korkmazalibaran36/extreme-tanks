﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using Photon.Pun;

public class Kontrol2v2 : MonoBehaviour
{
    public Camera maincam;
    public float hiz, donme;
    public bool ileribool, sagbool, solbool, ilerigeri, speedbl, kaybolmabool;
    public GameObject ilerivites, gerivites, butonpower1, powerspeedgo, nitro, karakter, karaktermermi, karakterboru, isimtxt, kaybolmus, mygun, efektler;
    public int skin, pingint = 10, hizint, speed5, kaybolmaint, fx;
    public Rigidbody2D rb2d;
    public Text pingtxt, playertext;
    public Slider sliderofme;


    void Start()
    {
        fx = PlayerPrefs.GetInt("fx");
        Debug.Log("this is no:" + this.GetComponent<PhotonView>().Owner.ActorNumber + " " + this.GetComponent<PhotonView>().IsMine);
        rb2d = this.GetComponent<Rigidbody2D>();
        skin = PlayerPrefs.GetInt("skin");
        ilerigeri = true;

        if (skin == 0)
        {
            hiz = 0.2f;
        }
        if (skin == 1)
        {
            hiz = 0.2f;
        }
        if (skin == 2)
        {
            hiz = 0.22f;
        }
        if (skin == 3)
        {
            hiz = 0.25f;
        }
        if (skin == 4)
        {
            hiz = 0.25f;
        }
        if (skin == 5)
        {
            hiz = 0.27f;
        }
        if (skin == 6)
        {
            hiz = 0.27f;
        }
        if (skin == 7)
        {
            hiz = 0.27f;
        }
        if (skin == 8)
        {
            hiz = 0.3f;
        }
        if (skin == 9)
        {
            hiz = 0.3f;
        }
        if (fx == 3)
        {
            hiz += 0.06f;
        }
        if (fx == 8)
        {
            hiz += 0.04f;
        }



    }
    public void powerspeed()
    {
        float money = PlayerPrefs.GetFloat("money");
        if (money >= 100)
        {
            hiz = hiz * 2f;
            money -= 100;
            PlayerPrefs.SetFloat("money", money);
            speedbl = true;
            hizint = 300;
        }
    }
    public void powerkaybolma()
    {
        float money = PlayerPrefs.GetFloat("money");
        if (money >= 50)
        {
            money -= 50;
            PlayerPrefs.SetFloat("money", money);
            this.GetComponent<PhotonView>().RPC("kaybolma", RpcTarget.AllBuffered, false);
            kaybolmabool = true;
            kaybolmaint = 300;
        }
    }


    void FixedUpdate()
    {
        donme = sliderofme.value;
        mygun.transform.localPosition = new Vector3((donme / 100), 0.32f, -3.4f);
        mygun.transform.localEulerAngles = new Vector3(0, 0, -donme);
        playertext.text = PhotonNetwork.CurrentRoom.PlayerCount.ToString() + "/20 Player(s)";
        if (kaybolmabool)
        {
            kaybolmus.SetActive(true);
            kaybolmaint--;
            if (kaybolmaint <= 0)
            {
                kaybolmus.SetActive(false);
                this.GetComponent<PhotonView>().RPC("kaybolma", RpcTarget.AllBuffered, true);
                kaybolmabool = false;
            }
        }
        if (speedbl)
        {
            powerspeedgo.SetActive(true);
            nitro.gameObject.SetActive(true);
            hizint--;
            butonpower1.SetActive(false);

            if (hizint <= 0)
            {
                hiz = hiz / 2;
                powerspeedgo.SetActive(false);
                nitro.SetActive(false);
                speedbl = false;

            }
        }
        pingint--;
        if (pingint <= 0)
        {
            pingtxt.text = "ping: " + PhotonNetwork.GetPing().ToString() + " ms";
            pingint = 100;
        }
        // float moveHorizontal = Input.GetAxis("Horizontal");

        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2(0, moveVertical);
        this.transform.Translate(movement * (hiz));

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, 0, 1.5f);

        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 0, -1.5f);

        }


        if (ileribool)
        {
            if (ilerigeri == true)
            {
                transform.Translate(0, hiz, 0);

            }
            else
            {
                transform.Translate(0, -hiz / 1.5f, 0);

            }
        }
        if (sagbool)
        {
            transform.Rotate(0, 0, -1.5f);
        }
        if (solbool)
        {
            transform.Rotate(0, 0, 1.5f);
        }


    }
    public void ileri()
    {
        ileribool = true;
    }
    public void ileribirak()
    {
        ileribool = false;
    }
    public void solbirak()
    {
        solbool = false;
    }
    public void sagbirak()
    {
        sagbool = false;
    }

    public void donmesag()
    {
        sagbool = true;
    }
    public void donmesol()
    {
        solbool = true;
    }
    public void vitesayarla()
    {
        if (ilerigeri == true)
        {
            gerivites.gameObject.SetActive(true);
            ilerivites.gameObject.SetActive(false);
            ilerigeri = false;
        }
        else if (ilerigeri == false)
        {
            gerivites.gameObject.SetActive(false);
            ilerivites.gameObject.SetActive(true);
            ilerigeri = true;
        }
    }
    [PunRPC]
    public void kaybolma(bool acikmi)
    {
        karakterboru.SetActive(acikmi);
        efektler.SetActive(acikmi);
        karaktermermi.SetActive(acikmi);
        karakter.GetComponent<SpriteRenderer>().enabled = acikmi;
        isimtxt.SetActive(acikmi);

    }
    public void Quit()
    {
        PhotonNetwork.LeaveRoom();
        Application.LoadLevel("MainMenu");
    }
}

