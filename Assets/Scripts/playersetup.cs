﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class playersetup : MonoBehaviourPunCallbacks
{

    public GameObject tank, roket, canvasgo;

    void Start()
    {



    }

    void FixedUpdate()
    {

        if (photonView.IsMine)
        {
            tank.GetComponent<Karakter>().enabled = true;
            tank.GetComponent<KarakterKontrol>().enabled = true;
            tank.GetComponent<KarakterKontrol>().maincam.enabled = true;
            canvasgo.SetActive(true);
            roket.GetComponent<roket>().enabled = true;


        }
        else
        {
            tank.GetComponent<KarakterKontrol>().enabled = false;

            tank.GetComponent<Karakter>().enabled = false;
            tank.GetComponent<KarakterKontrol>().maincam.enabled = false;
            canvasgo.SetActive(false);
            roket.GetComponent<roket>().enabled = false;



        }
    }
}
